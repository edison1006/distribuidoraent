<h1 class="text-center">NUEVO PROVEEDOR</h1>
<form class="" action="<?php echo site_url(); ?>/proveedores/guardar" method="post">
  <div class="container">
    <div class="row">
        <div class="col-md-6">
          <label for="">Ruc del proveedor</label>
          <br>
          <input type="text" placeholder="Ingrese el ruc del proveedor" class="form-control" name="ruc_prov" value="" id="ruc_prov">
        </div>
        <div class="col-md-6">
          <label for="">Nombre del proveedor</label>
          <br>
          <input type="text" placeholder="Ingrese el nombre del proveedor" class="form-control" name="nombre_prov" value="" id="nombre_prov">
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-6">
          <label for="">Teléfono del proveedor</label>
          <br>
          <input type="text" placeholder="Ingrese el teléfono del proveedor" class="form-control" name="telefono_prov" value="" id="telefono_prov">
        </div>
        <div class="col-md-6">
          <label for="">Dirección del proveedor</label>
          <br>
          <input type="text" placeholder="Ingrese la dirección del proveedor" class="form-control" name="direccion_prov" value="" id="direccion_prov">
        </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-12">
        <label for="">E-mail del proveedor</label>
        <br>
        <input type="text" placeholder="Ingrese el e-mail del proveedor" class="form-control" name="email_prov" value="" id="email_prov">
      </div>

    </div>
    <br><br>
    <div class="col-md-12 text-center">
      <button type="submit" name="button" class="btn btn-primary">GUARDAR</button>
      &nbsp
      <a href="<?php echo site_url(); ?>/proveedores/listarproveedor" class="btn btn-danger">CANCELAR</a>
    </div>
  </div>
</form>
<br><br><br><br>
