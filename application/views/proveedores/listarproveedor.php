<style>
.nuevo{
  margin-top: 18px;
}
</style>
<div class="container">
  <div class="row">
    <div class="col-md-8">
      <h1 class="text-center">LISTADO DE PROVEEDORES</h1>
    </div>
    <div class="col-md-4 nuevo">
      <a href="<?php echo site_url('proveedores/nuevoproveedor') ?>" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i>Agregar Proveedor</a>
    </div>
  </div>
  <br>
  <?php if ($proveedores): ?>
    <table class="table table-striped table-bordered table-hover">
      <thead>
        <tr>
          <th>ID</th>
          <th>RUC DEL PROVEEDOR</th>
          <th>NOMBRE DEL PROVEEDOR</th>
          <th>TELÉFONO DEL PROVEEDOR</th>
          <th>DIRECCIÓN DEL PROVEEDOR</th>
          <th>E-MAIL DEL PROVEEDOR</th>
          <th>ACCIONES</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($proveedores as $filaTemporal): ?>
          <tr>
            <td>
              <?php echo $filaTemporal->id_prov ?>
            </td>
            <td>
              <?php echo $filaTemporal->ruc_prov ?>
            </td>
            <td>
              <?php echo $filaTemporal->nombre_prov ?>
            </td>
            <td>
              <?php echo $filaTemporal-> telefono_prov?>
            </td>
            <td>
              <?php echo $filaTemporal->direccion_prov ?>
            </td>
            <td>
              <?php echo $filaTemporal-> email_prov?>
            </td>
            <td class="text-center">
              <a href="#" title="Editar Proveedor" style="color:orange;"> <i class="glyphicon glyphicon-pencil"></i></a>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <a href="<?php echo site_url(); ?>/proveedores/eliminar/<?php echo $filaTemporal->id_prov ?>"title="Eliminar Proveedor" onclick="return confirm('¿Estas seguro de eliminar el registro?');" style="color:red;"> <i class="glyphicon glyphicon-trash"></i></a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    <br><br>
  <?php else: ?>
    <h1>No hay proveedores</h1>
  <?php endif; ?>
</div>
