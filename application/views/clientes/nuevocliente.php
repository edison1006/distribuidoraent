<h1 class="text-center">NUEVO CLIENTE</h1>
<form class="" action="<?php echo site_url(); ?>/clientes/guardar" method="post">
  <div class="container">
    <div class="row">
        <div class="col-md-6">
          <label for="">Cedula del cliente</label>
          <br>
          <input type="text" placeholder="Ingrese la cedula del ciente" class="form-control" name="cedula_cli" value="" id="cedula_cli">
        </div>
        <div class="col-md-6">
          <label for="">Apellidos del Cliente</label>
          <br>
          <input type="text" placeholder="Ingrese los apellido del cliente" class="form-control" name="apellidos_cli" value="" id="apellidos_cli">
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-6">
          <label for="">Nombres del cliente</label>
          <br>
          <input type="text" placeholder="Ingrese los nombres del cliente" class="form-control" name="nombres_cli" value="" id="nombres_cli">
        </div>
        <div class="col-md-6">
          <label for="">Teléfono del cliente</label>
          <br>
          <input type="text" placeholder="Ingrese el teléfono del cliente" class="form-control" name="telefono_cli" value="" id="telefono_cli">
        </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-6">
        <label for="">Dirección del cliente</label>
        <br>
        <input type="text" placeholder="Ingrese la direccion del cliente" class="form-control" name="direccion_cli" value="" id="direccion_cli">
      </div>
      <div class="col-md-6">
        <label for="">E-mail del cliente</label>
        <br>
        <input type="text" placeholder="Ingrese el e-mail del cliente" class="form-control" name="email_cli" value="" id="email_cli">
      </div>
    </div>
    <br><br>
    <div class="col-md-12 text-center">
      <button type="submit" name="button" class="btn btn-primary">GUARDAR</button>
      &nbsp
      <a href="<?php echo site_url(); ?>/clientes/listarcliente" class="btn btn-danger">CANCELAR</a>
    </div>
  </div>
</form>
<br><br><br><br>
