<style>
.nuevo{
  margin-top: 18px;
}
</style>
<div class="container">
  <div class="row">
    <div class="col-md-8">
      <h1 class="text-center">LISTADO DE CLIENTES</h1>
    </div>
    <div class="col-md-4 nuevo">
      <a href="<?php echo site_url('clientes/nuevocliente') ?>" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i>Agregar Cliente</a>
    </div>
  </div>
  <br>
  <?php if ($clientes): ?>
    <table class="table table-striped table-bordered table-hover">
      <thead>
        <tr>
          <th>ID</th>
          <th>CEDULA DEL CLIENTE</th>
          <th>APELLIDOS DEL CLIENTE</th>
          <th>NOMBRES DEL CLIENTE</th>
          <th>TELÉFONO DEL CLIENTE</th>
          <th>DIRECCIÓN DEL CLIENTE</th>
          <th>E-MAIL DEL CLIENTE</th>
          <th>ACCIONES</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($clientes as $filaTemporal): ?>
          <tr>
            <td>
              <?php echo $filaTemporal->id_cli ?>
            </td>
            <td>
              <?php echo $filaTemporal->cedula_cli ?>
            </td>
            <td>
              <?php echo $filaTemporal-> apellidos_cli?>
            </td>
            <td>
              <?php echo $filaTemporal->nombres_cli ?>
            </td>
            <td>
              <?php echo $filaTemporal-> telefono_cli?>
            </td>
            <td>
              <?php echo $filaTemporal-> direccion_cli?>
            </td>
            <td>
              <?php echo $filaTemporal-> email_cli?>
            </td>
            <td class="text-center">
              <a href="#" title="Editar Cliente" style="color:orange;"> <i class="glyphicon glyphicon-pencil"></i></a>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <a href="<?php echo site_url(); ?>/clientes/eliminar/<?php echo $filaTemporal->id_cli ?>"title="Eliminar Cliente" onclick="return confirm('¿Estas seguro de eliminar el registro?');" style="color:red;"> <i class="glyphicon glyphicon-trash"></i></a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    <br><br>
  <?php else: ?>
    <h1>No hay clientes</h1>
  <?php endif; ?>
</div>
