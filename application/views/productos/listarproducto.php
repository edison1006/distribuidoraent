<style>
.nuevo{
  margin-top: 18px;
}
</style>
<div class="container">
  <div class="row">
    <div class="col-md-8">
      <h1 class="text-center">LISTADO DE PRODUCTOS</h1>
    </div>
    <div class="col-md-4 nuevo">
      <a href="<?php echo site_url('productos/nuevoproducto') ?>" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i>Agregar Productos</a>
    </div>
  </div>
  <br>
  <?php if ($productos): ?>
    <table class="table table-striped table-bordered table-hover">
      <thead>
        <tr>
          <th>ID</th>
          <th>NOMBRE DEL PRODUCTO</th>
          <th>DESCRIPCIÓN DEL PRODUCTO</th>
          <th>PRECIO UNITARIO</th>
          <th>PRECIO POR DOCENA</th>
          <th>CANTIDAD DEL PRODUCTO</th>
          <th>ACCIONES</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($productos as $filaTemporal): ?>
          <tr>
            <td>
              <?php echo $filaTemporal->id_pro ?>
            </td>
            <td>
              <?php echo $filaTemporal->nombre_pro ?>
            </td>
            <td>
              <?php echo $filaTemporal-> descripcion_pro?>
            </td>
            <td>
              <?php echo $filaTemporal->precio_uni_pro ?>
            </td>
            <td>
              <?php echo $filaTemporal-> precio_doce_pro?>
            </td>
            <td>
              <?php echo $filaTemporal-> cantidad_pro?>
            </td>
            <td class="text-center">
              <a href="#" title="Editar Producto" style="color:orange;"> <i class="glyphicon glyphicon-pencil"></i></a>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <a href="<?php echo site_url(); ?>/productos/eliminar/<?php echo $filaTemporal->id_pro ?>"title="Eliminar Producto" onclick="return confirm('¿Estas seguro de eliminar el registro?');" style="color:red;"> <i class="glyphicon glyphicon-trash"></i></a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    <br><br>
  <?php else: ?>
    <h1>No hay productos</h1>
  <?php endif; ?>
</div>
