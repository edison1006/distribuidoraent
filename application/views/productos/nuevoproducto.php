<h1 class="text-center">NUEVO PRODUCTO</h1>
<form class="" action="<?php echo site_url(); ?>/productos/guardar" method="post">
  <div class="container">
    <div class="row">
        <div class="col-md-6">
          <label for="">Nombre del producto</label>
          <br>
          <input type="text" placeholder="Ingrese el nombre del producto" class="form-control" name="nombre_pro" value="" id="nombre_pro">
        </div>
        <div class="col-md-6">
          <label for="">Descripción del producto</label>
          <br>
          <input type="text" placeholder="Ingrese la descripción del producto" class="form-control" name="descripcion_pro" value="" id="descripcion_pro">
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-6">
          <label for="">Precio del producto por unidad</label>
          <br>
          <input type="text" placeholder="Ingrese el precio por unidad" class="form-control" name="precio_uni_pro" value="" id="precio_uni_pro">
        </div>
        <div class="col-md-6">
          <label for="">Precio del producto por docena</label>
          <br>
          <input type="text" placeholder="Ingrese el precio por docena" class="form-control" name="precio_doce_pro" value="" id="precio_doce_pro">
        </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-12">
        <label for="">Cantidad del producto</label>
        <br>
        <input type="text" placeholder="Ingrese la cantidad del producto" class="form-control" name="cantidad_pro" value="" id="cantidad_pro">
      </div>
    </div>
    <br><br>
    <div class="col-md-12 text-center">
      <button type="submit" name="button" class="btn btn-primary">GUARDAR</button>
      &nbsp
      <a href="<?php echo site_url(); ?>/productos/listarproducto" class="btn btn-danger">CANCELAR</a>
    </div>
  </div>
</form>
<br><br><br><br>
