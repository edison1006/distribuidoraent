<?php
class Contenedores extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
  }
  //funcion para renderizar las vistas
  public function nosotros(){
    $this->load->view('header');
    $this->load->view('contenedores/nosotros');
    $this->load->view('footer');
  }

  public function ubicanos(){
    $this->load->view('header');
    $this->load->view('contenedores/ubicanos');
    $this->load->view('footer');
  }

}

?>
