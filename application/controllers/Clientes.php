<?php
class Clientes extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Cliente');
  }
  //funcion para renderizar las vistas
  public function nuevocliente(){
    $this->load->view('header');
    $this->load->view('clientes/nuevocliente');
    $this->load->view('footer');
  }

  public function listarcliente(){
    $data['clientes']=$this->Cliente->obtenerTodos();
    $this->load->view('header');
    $this->load->view('clientes/listarcliente',$data);
    $this->load->view('footer');
  }
  public function guardar(){
  //codigo neto
  $datosNuevoCliente=array(
    "cedula_cli"=>$this->input->post('cedula_cli'),
    "apellidos_cli"=>$this->input->post('apellidos_cli'),
    "nombres_cli"=>$this->input->post('nombres_cli'),
    "telefono_cli"=>$this->input->post('telefono_cli'),
    "direccion_cli"=>$this->input->post('direccion_cli'),
    "email_cli"=>$this->input->post('email_cli')
  );
  if ($this->Cliente->insertar($datosNuevoCliente)) {
    redirect('clientes/listarcliente');
  }else {
      echo "<h1>ERROR AL INSERTAR</h1>";
    }
  }
  //funcion para eliminar instructores
  public function eliminar($id_cli){
    if ($this->Cliente->borrar($id_cli))
    {//invocando el modelo
      redirect('clientes/listarcliente');
    }else{
      echo "ERROR AL BORRAR :()";
    }
  }
}

?>
