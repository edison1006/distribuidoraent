<?php
class Proveedores extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Proveedor');
  }
  //funcion para renderizar las vistas
  public function listarproveedor(){
    $data['proveedores']=$this->Proveedor->obtenerTodos();
    $this->load->view('header');
    $this->load->view('proveedores/listarproveedor',$data);
    $this->load->view('footer');
  }
  public function nuevoproveedor(){
    $this->load->view('header');
    $this->load->view('proveedores/nuevoproveedor');
    $this->load->view('footer');
  }
  public function guardar(){
    //codigo neto
    $datosNuevoProveedor=array(
      "ruc_prov"=>$this->input->post('ruc_prov'),
      "nombre_prov"=>$this->input->post('nombre_prov'),
      "telefono_prov"=>$this->input->post('telefono_prov'),
      "direccion_prov"=>$this->input->post('direccion_prov'),
      "email_prov"=>$this->input->post('email_prov')
    );
    if ($this->Proveedor->insertar($datosNuevoProveedor)) {
      redirect('proveedores/listarproveedor');
    }else {
        echo "<h1>ERROR AL INSERTAR</h1>";
      }
    }
    //funcion para eliminar instructores
    public function eliminar($id_prov){
      if ($this->Proveedor->borrar($id_prov))
      {//invocando el modelo
        redirect('proveedores/listarproveedor');
      }else{
        echo "ERROR AL BORRAR :()";
      }
    }
}

?>
