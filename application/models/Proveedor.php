<?php

class Proveedor extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }
  //funcion para insertar un instructor
  function insertar($datos){
    //CONSULTAR ACTIVE RECORD -> CodeIgniter Inyeccion SQL
    return $this->db->insert("proveedor",$datos);
  }
  //Funcion para consultar productos
  function obtenerTodos(){
    $listadoProveedores=$this->db->get("proveedor");
    if($listadoProveedores->num_rows()>0){//Si hay datos
    return $listadoProveedores->result();
      }else{
        return false;
      }
  }
  //borrar instructores
  function borrar($id_prov)
  {
    $this->db->where("id_prov",$id_prov);
    return $this->db->delete("proveedor");
  }


}//cierre de la clase

?>
