<?php

class CLiente extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }
  //funcion para insertar un instructor
  function insertar($datos){
    //CONSULTAR ACTIVE RECORD -> CodeIgniter Inyeccion SQL
    return $this->db->insert("cliente",$datos);
  }
  //Funcion para consultar productos
  function obtenerTodos(){
    $listadoClientes=$this->db->get("cliente");
    if($listadoClientes->num_rows()>0){//Si hay datos
    return $listadoClientes->result();
      }else{
        return false;
      }
  }
  //borrar instructores
  function borrar($id_cli)
  {
    $this->db->where("id_cli",$id_cli);
    return $this->db->delete("cliente");
  }
}//cierre de la clase

?>
